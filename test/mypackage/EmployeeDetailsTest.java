package mypackage;

import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by admin on 03/03/2017.
 */
public class EmployeeDetailsTest {

    private ArrayList<Employee> employeeArrayList = new ArrayList<>();


    private void createEmployees(){
        Employee john = new Employee(10,"7546337m","Lennon","John",'M',"Music",888888,true);
        Employee ringo = new Employee(31,"6888837l","Starr","Ringo",'M',"Music",77777.50,true);
        employeeArrayList.add(john);
        employeeArrayList.add(ringo);
    }


    @Test
    public void searchEmployeeById() throws Exception {
        createEmployees();
        JTextField searchField = new JTextField();

        searchField.setText("31");
        Employee anEmployee = null;

        if(employeeArrayList != null){
            for(Employee emp: employeeArrayList){
                if (Integer.parseInt(searchField.getText().trim()) == emp.getEmployeeId()) {
                    anEmployee = emp;
                }
            }
        }
        System.out.println("Employee id from employee object is : "+anEmployee.getEmployeeId());
        System.out.println("Value entered in the JTextField is  : "+searchField.getText());
        assertNotNull(anEmployee);
    }

    @Test
    public void searchEmployeeBySurname() throws Exception {
        createEmployees();
        JTextField searchField = new JTextField();

        searchField.setText("Lennon");
        Employee anEmployee = null;

        if(employeeArrayList != null){

            for(Employee emp: employeeArrayList){
                if (searchField.getText().trim().equalsIgnoreCase(emp.getSurname()) ){
                    anEmployee = emp;
                }
            }
        }
        System.out.println("Employee Surname from employee object is : "+anEmployee.getSurname());
        System.out.println("Value entered in the JTextField is  : "+searchField.getText());

        assertNotNull(anEmployee);
    }

    @Test
    public void addRecord() throws Exception {

    }

}