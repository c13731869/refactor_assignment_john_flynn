package mypackage;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;

import static mypackage.GlobalVariableConfig.BACKGROUND_COLOR;
import static mypackage.GlobalVariableConfig.DETAILS_LABEL_CONSTRAINTS;
import static mypackage.GlobalVariableConfig.DETAILS_PANEL_CONSTRAINTS;


class AddRecordDialog extends JDialog implements ActionListener {
    private JTextField idField, ppsField, surnameField, firstNameField, salaryField;
    private JComboBox<String> genderCombo, departmentCombo, fullTimeCombo;
    private JButton save, cancel;
    private final EmployeeDetails parent;




    AddRecordDialog(EmployeeDetails parent) {
        setTitle("Add Record");
        setModal(true);
        this.parent = parent;
        this.parent.setEnabled(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JScrollPane scrollPane = new JScrollPane(dialogPane());
        setContentPane(scrollPane);

        getRootPane().setDefaultButton(save);

        setSize(500, 370);
        setLocation(350, 250);
        setVisible(true);
    }

    private Container dialogPane() {
        JPanel empDetails, buttonPanel;
        empDetails = new JPanel(new MigLayout());
        buttonPanel = new JPanel();
        JTextField field;

        empDetails.setBorder(BorderFactory.createTitledBorder("mypackage.Employee Details"));

        empDetails.add(new JLabel("ID:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(idField = new JTextField(20), DETAILS_PANEL_CONSTRAINTS);
        idField.setEditable(false);


        empDetails.add(new JLabel("PPS Number:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(ppsField = new JTextField(20), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("Surname:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(surnameField = new JTextField(20), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("First Name:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(firstNameField = new JTextField(20), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("Gender:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(genderCombo = new JComboBox<>(this.parent.GENDER), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("Department:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(departmentCombo = new JComboBox<>(this.parent.DEPARTMENT), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("Salary:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(salaryField = new JTextField(20), DETAILS_PANEL_CONSTRAINTS);

        empDetails.add(new JLabel("Full Time:"), DETAILS_LABEL_CONSTRAINTS);
        empDetails.add(fullTimeCombo = new JComboBox<>(this.parent.FULLTIME), DETAILS_PANEL_CONSTRAINTS);

        buttonPanel.add(save = new JButton("Save"));
        save.addActionListener(this);
        save.requestFocus();
        buttonPanel.add(cancel = new JButton("Cancel"));
        cancel.addActionListener(this);

        empDetails.add(buttonPanel, "span 2,growx, pushx,wrap");

        for (int i = 0; i < empDetails.getComponentCount(); i++) {
            empDetails.getComponent(i).setFont(this.parent.FONT1);
            if (empDetails.getComponent(i) instanceof JComboBox) {
                empDetails.getComponent(i).setBackground(Color.WHITE);
            } else if (empDetails.getComponent(i) instanceof JTextField) {
                field = (JTextField) empDetails.getComponent(i);
                if (field == ppsField)
                    field.setDocument(new JTextFieldLimit(9));
                else
                    field.setDocument(new JTextFieldLimit(20));
            }
        }
        idField.setText(Integer.toString(this.parent.getNextFreeId()));
        return empDetails;
    }

    private void addRecord() {
        boolean fullTime = false;
        Employee theEmployee;

        if (((String) fullTimeCombo.getSelectedItem()).equalsIgnoreCase("Yes"))
            fullTime = true;

        theEmployee = new Employee(Integer.parseInt(idField.getText()), ppsField.getText().toUpperCase(), surnameField.getText().toUpperCase(),
                firstNameField.getText().toUpperCase(), genderCombo.getSelectedItem().toString().charAt(0),
                departmentCombo.getSelectedItem().toString(), Double.parseDouble(salaryField.getText()), fullTime);
        this.parent.currentEmployee = theEmployee;
        this.parent.addRecord(theEmployee);
        this.parent.displayRecords(theEmployee);
    }

    private boolean checkInput() {
        boolean valid = true;

        if (ppsField.getText().equals("")) {
            ppsField.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (this.parent.correctPps(this.ppsField.getText().trim())) {
            ppsField.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (surnameField.getText().isEmpty()) {
            surnameField.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (firstNameField.getText().isEmpty()) {
            firstNameField.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (genderCombo.getSelectedIndex() == 0) {
            genderCombo.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (departmentCombo.getSelectedIndex() == 0) {
            departmentCombo.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        try {

            if (Double.parseDouble(salaryField.getText()) < 0) {
                salaryField.setBackground(BACKGROUND_COLOR);
                valid = false;
            }
        } catch (NumberFormatException num) {
            salaryField.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        if (fullTimeCombo.getSelectedIndex() == 0) {
            fullTimeCombo.setBackground(BACKGROUND_COLOR);
            valid = false;
        }
        return valid;
    }

    private void setToWhite() {

        ppsField.setBackground(Color.WHITE);
        surnameField.setBackground(Color.WHITE);
        firstNameField.setBackground(Color.WHITE);
        salaryField.setBackground(Color.WHITE);
        genderCombo.setBackground(Color.WHITE);
        departmentCombo.setBackground(Color.WHITE);
        fullTimeCombo.setBackground(Color.WHITE);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == save) {
            if (checkInput()) {
                addRecord();
                dispose();
                this.parent.changesMade = true;
            }
            else {
                JOptionPane.showMessageDialog(null, "Wrong values or format! Please check!");
                setToWhite();
            }
        } else if (e.getSource() == cancel)
            dispose();
    }

}