package mypackage;/*
 * 
 * This is the dialog for mypackage.Employee search by ID
 * 
 * */



import javax.swing.*;



class SearchByIdDialog extends JDialog {
    private JButton search, cancel;
    private JTextField searchField;

    SearchByIdDialog(EmployeeDetails parent) {

        setTitle("Search by ID now");
        setModal(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        SearchDialogByParam searchDialogByParam = new SearchDialogByParam(parent,search,cancel,searchField,"ID");

        JScrollPane scrollPane = new JScrollPane(searchDialogByParam.searchPane());
        setContentPane(scrollPane);

        getRootPane().setDefaultButton(search);

        setSize(500, 190);
        setLocation(350, 250);
        setVisible(true);
    }
}