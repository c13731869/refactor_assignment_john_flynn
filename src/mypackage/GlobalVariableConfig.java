package mypackage;

import javax.swing.*;
import java.awt.*;


class GlobalVariableConfig {

    static final String DETAILS_PANEL_CONSTRAINTS = "growx, pushx, wrap";
    static final String DETAILS_LABEL_CONSTRAINTS = "growx, pushx";
    static final Color BACKGROUND_COLOR =    new Color(255, 150, 150);
    static final Color BACKGROUND_WHITE =  UIManager.getColor("TextField.background");

}
