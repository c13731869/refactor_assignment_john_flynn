package mypackage;


import javax.swing.*;

class SearchBySurnameDialog extends JDialog{
    private JButton search, cancel;
    private JTextField searchField;

    SearchBySurnameDialog(EmployeeDetails parent) {
        setTitle("Search by Surname now");
        setModal(true);
        EmployeeDetails PARENT = parent;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        SearchDialogByParam searchDialogByParam = new SearchDialogByParam(parent,search,cancel,searchField,"Surname");

        JScrollPane scrollPane = new JScrollPane(searchDialogByParam.searchPane());
        setContentPane(scrollPane);

        getRootPane().setDefaultButton(search);

        setSize(500, 190);
        setLocation(350, 250);
        setVisible(true);
    }
}